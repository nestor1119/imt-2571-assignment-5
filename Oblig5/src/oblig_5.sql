-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2017 at 10:08 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oblig 5`
--

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `id` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(250) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `representing`
--

CREATE TABLE `representing` (
  `uName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `sYear` int(4) NOT NULL,
  `cId` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `totalDistance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE `season` (
  `sYear` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skier`
--

CREATE TABLE `skier` (
  `uName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `fName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `lName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `representing`
--
ALTER TABLE `representing`
  ADD PRIMARY KEY (`uName`,`sYear`,`cId`),
  ADD KEY `representing 1cid` (`cId`),
  ADD KEY `representing 2syear` (`sYear`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`sYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`uName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `representing`
--
ALTER TABLE `representing`
  ADD CONSTRAINT `representing 1cid` FOREIGN KEY (`cId`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `representing 2syear` FOREIGN KEY (`sYear`) REFERENCES `season` (`sYear`),
  ADD CONSTRAINT `representing 3uname` FOREIGN KEY (`uName`) REFERENCES `skier` (`uName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
